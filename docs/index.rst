.. PyPrint documentation master file, created by
   sphinx-quickstart on Tue Aug  7 16:43:33 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyPrint's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2

.. toctree::
   :caption: PyPrint API Documentation
   :maxdepth: 4

   pyprint


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

